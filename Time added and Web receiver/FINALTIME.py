from machine import RTC
import network
import ntptime
import time

station = network.WLAN(network.STA_IF)

def connect(id, pswd):
    ssid = id
    password = pswd
    if station.isconnected():
        print("Already connected")
        return
    station.active(True)
    station.connect(ssid, password)
    while not station.isconnected():
        pass
    print("Connection successful")
    print(station.ifconfig())
    # Set the time when the device is connected to the network
    set_network_time()

def set_network_time():
    rtc = RTC()
    ntptime.settime()
    print("Network time set successfully")

def timesetting():
    rtc = RTC()
    (year, month, day, weekday, hours, minutes, seconds, subseconds) = rtc.datetime()
    
    hours += 5  # Add 5 hours for UTC+5
    minutes += 30  # Add 30 minutes for UTC+5:30
    if minutes >= 60:
        minutes -= 60
        hours += 1
    if hours >= 24:
        hours -= 24
        day += 1

    Date = "Date:{}.{}.{}".format(day, month, year)
    Time = "Time {}:{}:{}".format(hours, minutes, seconds)
    
    return Date, Time

connect('Selfmade Ninja', 'Ninja@021')
print(timesetting())

