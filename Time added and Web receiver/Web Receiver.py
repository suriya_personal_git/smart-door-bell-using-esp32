from paho.mqtt import client as mqtt_client

broker = 'rabbitmq.selfmade.ninja'
port = 1883
topic = "Smart_Door_Bell"
client_id = "Web_Receiver"
username="SuriyaBharathwaj_ESP_SERVER:Sender"
password="stegil&9"

print("Welcome")

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        print("connecting")
        if rc == 0:
            print("Connected to MQTT Broker!{}".format(rc))
        else:
            print("Failed to connect, return code %d\n", rc)
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def subscribe(client):
    def on_message(client, userdata, msg):
        print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")

    client.subscribe(topic)
    client.on_message = on_message

# if __name__ == "__main__":
#     mqtt_client = connect_mqtt()
#     mqtt_client.loop_start()  # Start the MQTT client loop
#     subscribe(mqtt_client)
#     print("Start")
    
if __name__ == "__main__":
    mqtt_client = connect_mqtt()
    subscribe(mqtt_client)
    try:
        print("Loop Started")
        mqtt_client.loop_forever()  # Start the MQTT client loop
    except KeyboardInterrupt as e:
        print(e)
