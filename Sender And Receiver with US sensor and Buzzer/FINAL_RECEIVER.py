import network
import socket
from machine import Pin, PWM
import time

# Wi-Fi credentials
ssid = 'Selfmade Ninja'
password = 'Ninja@021'

buzzer = PWM(Pin(25, Pin.OUT), freq=440, duty=512)
buzzer.deinit()

def beep(duration):
    buzzer.duty(512)
    time.sleep(duration)
    buzzer.duty(0)
    time.sleep(duration)

# Connect to Wi-Fi as a station (client)
station = network.WLAN(network.STA_IF)
station.active(True)
station.connect(ssid, password)

# Wait for connection
while not station.isconnected():
    pass

print('Connected to Wi-Fi')
print('Network configuration:', station.ifconfig())

# Create a socket and listen for connections
port = 12345
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('', port))
sock.listen(1)

print('Listening for connections...')

while True:
    conn, addr = sock.accept()
    print('Connection from', addr)

    try:
        while True:
            # Receive the message
            message = conn.recv(1024).decode()
            if not message:
                break

            print('Message received:', message)
            
            if message:
               buzzer.init()
               for _ in range(3):
                  beep(0.4)
                

            # Add code to output the message through the speaker here
            # This part will depend on your specific speaker setup

    except Exception as e:
        print('An error occurred:', e)

    finally:
        # Close the connection
        conn.close()