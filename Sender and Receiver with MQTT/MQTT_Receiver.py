from umqtt.simple import MQTTClient
import time
import network

# Connect to Wi-Fi first
def connect_to_wifi(ssid, password):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('Connecting to network...')
        wlan.connect(ssid, password)
        while not wlan.isconnected():
            pass
    print('Network config:', wlan.ifconfig())

# MQTT Configuration
MQTT_BROKER = "rabbitmq.selfmade.ninja"
CLIENT_ID = "your_client_id"
TOPIC = "Topic"
USERNAME = "SuriyaBharathwaj_ESP_SERVER:Sender"
PASSWORD = "stegil&9"

# Callback function to handle messages
def on_message(topic, msg):
    print("Received message '%s' on topic '%s'" % (msg, topic))

# Set up MQTT client
def setup_mqtt_client():
    client = MQTTClient(CLIENT_ID, MQTT_BROKER, user=USERNAME, password=PASSWORD)
    client.set_callback(on_message)
    return client

# Connect to the MQTT broker
def connect_to_mqtt(client):
    try:
        client.connect()
        print("Connected to MQTT Broker:", MQTT_BROKER)
    except OSError as e:
        print("Failed to connect to MQTT Broker. Error:", e)
        raise

# Main function
def main():
    # Connect to Wi-Fi
    connect_to_wifi('Selfmade Ninja', 'Ninja@021')

    # Set up MQTT client
    client = setup_mqtt_client()

    # Connect to MQTT
    connect_to_mqtt(client)

    # Subscribe to topic
    client.subscribe(TOPIC)
    print("Subscribed to topic:", TOPIC)

    try:
        while True:
            # Check for new messages
            client.check_msg()
            time.sleep(1)
    except KeyboardInterrupt:
        print('Disconnected from MQTT broker')
        client.disconnect()

# Run the main function
main()
