from umqtt.Simple import MQTTClient
import time,network
from machine import Pin
import time

#This part of code connects the ESP32 with WIFI
def connect_to_wifi(ssid, password):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('Connecting to network...')
        wlan.connect(ssid, password)
        while not wlan.isconnected():
            pass
    print('Network config:', wlan.ifconfig())
    
connect_to_wifi('Selfmade Ninja', 'Ninja@021')

# Define pins for ultrasonic sensor
trig_pin = Pin(4, Pin.OUT)
echo_pin = Pin(5, Pin.IN)

def measure_distance():
    # Send a 10 microsecond pulse to trigger the ultrasonic sensor
    trig_pin.on()
    time.sleep_us(10)
    trig_pin.off()
    
    # Measure the duration of the echo pulse
    while not echo_pin.value():
        pulse_start = time.ticks_us()
        #print(pulse_start)
        
    while echo_pin.value():
        pulse_end = time.ticks_us()
        #print(pulse_end)
    
    # Calculate the pulse duration in microseconds
    pulse_duration = pulse_end - pulse_start
    
    # Convert the pulse duration to distance in centimeters
    # Speed of sound is approximately 34300 cm/s
    distance = (pulse_duration * 34300) / 2 / 1000000  # Convert to seconds and divide by 2 for one-way distance
    
    return distance

#This part of code contains the RABBITMQ Credentials
BROKER_ADDRESS = "rabbitmq.selfmade.ninja"
CLIENT_ID = "ESP32_Sen"
TOPIC = "Topic"
SENDER_USERNAME = "SuriyaBharathwaj_ESP_SERVER:Sender"
SENDER_PASSWORD = "stegil&9"

#This part of code connects the client to the MQTT Broker
client = MQTTClient(CLIENT_ID, BROKER_ADDRESS, user=SENDER_USERNAME,password=SENDER_PASSWORD)
client.connect()
    
time_count = 0

#while True:
    #message = "Hello, message count: {}".format(message_count)
    #client.publish(TOPIC, message)
    #print("Published message:", message)
    #message_count += 1
    #time.sleep(1)
while True:
    distance = measure_distance()
    print("Distance:", distance, "cm")
    time.sleep(1)# Wait for 1 second before taking another measurement
    if distance < 7:
        mess = "Activity Detected at distance:{}".format(distance)
        print(mess)
        client.publish(TOPIC,mess)
        time_count+=1
        print("COUNT NO:",time_count)
        if time_count>=10:
            message="Activity Detected for more than 10 seconds"
            client.publish(TOPIC,message)
            print("Published message:", message)
            time.sleep(3)
            Dist=measure_distance()
            if Dist > 7:
                print("yes it is enterring the loop {}".format(Dist))
                time_count=0
            else:
                print("no")
                #time.sleep(5)
        else:
            pass
    else:
        pass