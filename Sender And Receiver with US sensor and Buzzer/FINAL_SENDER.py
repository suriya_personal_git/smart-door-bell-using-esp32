import network 
import socket 
import time
import machine
 
# Wi-Fi credentials 
ssid = 'Selfmade Ninja' 
password = 'Ninja@021' 
 
# Connect to Wi-Fi 
station = network.WLAN(network.STA_IF) 
station.active(True) 
station.connect(ssid, password) 
 
# Wait for connection with timeout 
timeout = 10  # 10 seconds timeout 
start_time = time.time()

trigger_pin = machine.Pin(4, machine.Pin.OUT)
echo_pin = machine.Pin(5, machine.Pin.IN)

def measure_distance():
    trigger_pin.off()
    time.sleep_us(2)
    trigger_pin.on()
    time.sleep_us(10)
    trigger_pin.off()
    while echo_pin.value() == 0:
        signaloff = time.ticks_us()
    while echo_pin.value() == 1:
        signalon = time.ticks_us()
    timepassed = signalon - signaloff
    distance = (timepassed * 0.0343) / 2
    return distance


 
while not station.isconnected(): 
    if time.time() - start_time > timeout: 
        print("Failed to connect to Wi-Fi within timeout period") 
        break 
 
if station.isconnected(): 
    print('Connected to Wi-Fi') 
    print('Network configuration:', station.ifconfig()) 
 
    try: 
        # Create a socket and connect to the receiver 
        receiver_ip = '192.168.23.84'  # IP of the receiver ESP32 
        port = 12345 
 
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        sock.connect((receiver_ip, port))
        time_within_range_start = None
 
        while True:
            distance = measure_distance()
            print(distance)

            if distance <= 80:
                print(time.time(),time_within_range_start)
                if time_within_range_start is None:
                    time_within_range_start = time.time()
                elif time.time() - time_within_range_start >= 7:
                     message = "jawa"
                     sock.send(message.encode())
                     time_within_range_start = None
                     time.sleep(4)
                    
            else:
                time_within_range_start = None
            # Send a message 
           
            time.sleep(1)
 
        # Close the socket 
        sock.close() 
 
    except Exception as e: 
        print('An error occurred:', e) 
else: 
    print("Wi-Fi connection failed")

